Ziel des Projekts ist die Erstellung einer Java Android-Anwendung für EU GDPR Data Kommissionärs in der Rahmen des Interdisziplinäre Projekt des Studenten Gruppe der Hochschule Rhein Waal unter der Führung des Herrn Prof. Dr. Dieter Kopetz.

Softwareentwicklerin: [Kateryna Tkachenko](https://www.linkedin.com/in/katryna1/)

Softwareentwickler: [Artem Kozlenkov](https://www.linkedin.com/in/artemkozlenkov/)

Landing Webseite Link: 

[https://eudpapp.gitlab.io/](https://eudpapp.gitlab.io/)
