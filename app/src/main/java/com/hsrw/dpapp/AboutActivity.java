package com.hsrw.dpapp;

import android.os.Build;
import android.view.View;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class AboutActivity extends AppCompatActivity implements IHelper {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        addToolBar();
    }

    @Override
    public void addToolBar() {
        Toolbar AboutToolbar = (Toolbar) findViewById(R.id.toolbar);
        AboutToolbar.setTitle(getString(R.string.menu_button_about));
        AboutToolbar.setNavigationIcon(R.drawable.arrow);
        AboutToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
